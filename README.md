# Data Structures and Algorithms
In this respository, I attempt to explore many different data structures and 
algorithms, in a few different languages. Mostly I will be using the languages 
Python, Rust, Zig, and C. 

**Note**: Nothing is uploaded yet, so none of the links below work. For the 
moment just a demonstration on how navigating will work.

## Data Structures
- Linked List
    - Singly Linked List \[[Python](#)\] \[[Rust](#)\] \[[Zig](#)\] \[[C](#)\]
- Queue
- Trees
    - Binary Search Tree \[[Python](#)\] \[[Rust](#)\] \[[Zig](#)\] \[[C](#)\]


## Algorithms
### Sorting
- Insertion Sort
- Merge Sort
- Quick Sort
### Searching

